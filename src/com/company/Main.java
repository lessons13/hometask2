package com.company;

import java.util.Scanner;

public class Main {
    static Scanner scanner;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);
        PointList pointList = new PointList();
        int answer;

        do {
            System.out.print("|Введите координаты точки\n |x: ");
            double x = input();
            System.out.print(" |y: ");
            double y = input();
            pointList.addPoint(new Point(x, y));

            System.out.print("| Желаете добавить еще (1 - да; 2 - нет)? ");
            answer = scanner.nextInt();
            System.out.println("Ваш выбор: " + answer);
        } while (answer == 1);

        System.out.print("|Введите координаты центра\n |x: ");
        double x = input();
        System.out.print(" |y: ");
        double y = input();
        Point center = new Point(x, y);
        System.out.print("|Введите радиус окружности: ");
        double r = input();
        Circle circle = new Circle(center, r);

        System.out.println("Точки, лежащие внутри окружности: ");
        for (int i = 0; i < pointList.getNumberOfPoints(); i++) {
            Point p = pointList.getPoint(i);
            if (circle.containsPoint(p)) {
                System.out.println(p);
            }
        }
    }

    public static double input() {
        return  scanner.nextDouble();
    }
}
